.TH SECRECY 1 2020-11-13 GNU "User's Manual"
.SH NAME
.B secrecy
\- libsecrecy encryption and key management tool
.SH SYNOPSIS
.B secrecy createKey
.I cipher gpgid keyname
.PP
.B secrecy encrypt
.RI [ keyhash | keyname ]
.PP
.B secrecy decrypt
.PP
.B secrecy exportKey
.RI [ keyhash | keyname ]
.I gpgid
.PP
.B secrecy importKey
.I gpgid
.PP
.B secrecy listKeys
.PP
.B secrecy setDefaultKey
.I keyname
.SH DESCRIPTION
.PP
The
.B libsecrecy
distribution comes with a command line tool called
.BR secrecy .
This tool currently has seven subcommands:
.BR createKey ,
.BR encrypt ,
.BR decrypt ,
.BR exportKey ,
.BR importKey ,
.B listKeys
and
.BR setDefaultKey .
.PP
The program
.B secrecy
accepts several subcommands.
It is to be noted that, currently,
.B secrecy
has no arguments for reading and writing any file.
One has to rely on the shell capabilities to read and write plain files,
encrypted files,
and key exports,
using pipes and redirection operators.
Subcommands are the following:
.TP
\fBsecrecy createKey\fI cipher gpgid keyname\fR
create keys for the
.BR libsecrecy ,
where
.I cipher
can currently take the values
.BR AES128 ,
.B AES192
or
.BR AES256 ,
.I gpgid
needs to be a valid id (normally an email address) present as a secret key in
.BR gpg (1)'s
keyring,
which can be used for securely storing the AES key for use by
.BR libsecrecy ,
and
.I keyname
can be chosen as a human readable name for the key created,
for instance "mykey".
The program outputs a key hash in the form of a hexadecimal encoded string.
Either this key hash or the key name given can to be provided to the encrypt command of
.B secrecy
for encrypting files using the newly created key.
.TP
\fBsecrecy encrypt\fR [\fIkeyhash\fR|\fIkeyname\fR]
This subcommand allows one to encrypt files.
.IR keyhash / keyname
is either the hexadecimal string which was printed by
.B createKey
when creating the key,
or the name given to
.B createKey
when creating the key.
If the string provided is empty,
then the default key name is used if any has been set;
see
.BR setDefaultKey .
Note that this command needs to decrypt the key from it's
.BR gpg (1)
encoded form,
so you will need to provide the respective passphrase in some form.
.TP
\fBsecrecy decrypt\fR
This subcommand allows one to decrypt files.
It needs to decrypt the key from it's
.BR gpg (1)
encoded form,
so you will need to provide the respective passphrase in some form.
Note that you do not need to provide the
.I keyhash
for decryption as this information is provided inside the encrypted file.
.TP
\fBsecrecy exportKey\fR [\fIkeyname\fR|\fIkeyhash\fR] \fIgpgid\fR
Export to an encrypted transfer format,
for passing data on to third parties,
via the
.B exportKey
command of
.BR secrecy .
.I keyname
or
.I keyhash
are valid key name or hash respectively,
and
.I gpgid
is a string identifying the recipient of the key.
The public key of
.I gpgid
needs to be available in
.BR gpg 's
key database.
.TP
\fBsecrecry importKey \fIgpgid\fR
Import a key from the format produced by the
.B exportKey
command using the
.B importKey
command,
where
.I gpgid
designates the
.B gpg
key which will be used to locally encrypt the key for storing it in
.BR libsecrecy 's
database.
.TP
.B secrecy listKeys
List installed keys.  It prints a tabulation separated table such that the first column contains the key names and the second the respective key hash values.
.TP
\fBsecrecy setDefaultKey \fIkeyname\fR
Change the default key.  The default key is used when an empty
.I keyname
is used for running any command accepting a key name,
with the obvious exceptions of
.B createKey
and
.BR setDefaultKey .
.PP
.SH FILES
.PP
AES keys are stored encrypted using
.BR gpg (1)
via
.BR gpgme .
Each key is assigned a hash
.I H
value at creation time.
.I H
is computed as the SHA256 checksum of a randomly generated sequence.
Keys are stored and searched for in the directory set in the environment variable
.IR LIBSECRECY_KEYDIR .
If this variable is not set,
then the subdirectory
.B .libsecrecy
inside the current users home directory,
designated by the environment variable
.IR HOME ,
is used.
Inside this directory the key for hash
.I H
is stored in the file
.BR hash/H ,
otherwise said:
.BR hash/3E35C013C66C66B09E3E0B923451530C62D4346D9F5165906FC94B9B4D35E28E ,
where the respective files are encrypted using
.BR gpgme .
The secret key used for this encryption can be set at key creation time.
.PP
.SH EXAMPLES
.PP
Create an
.I AES256
key using your GPG ID,
for instance
.IR foo@example.org ,
and call it
.IR mykey :
.PP
.EX
        $ secrecy createKey AES256 foo@example.org mykey
        3E35C013C66C66B09E3E0B923451530C62D4346D9F5165906FC94B9B4D35E28E
.EE
.PP
List available keys:
.PP
.EX
        $ secrecy listKeys
        mykey	3E35C013C66C66B09E3E0B923451530C62D4346D9F5165906FC94B9B4D35E28E
.EE
.PP
Set
.I mykey
as default key:
.PP
.EX
        $ secrecy setDefaultKey mykey
.EE
.PP
Encrypt
.I file
into
.IR file.encrypted ,
using the default key:
.PP
.EX
        $ secrecy encrypt < file > file.encrypted
.EE
.PP
Decrypt
.I file.encrypted
into
.IR file.decrypted ,
using the default key:
.PP
.EX
        $ secrecy decrypt < file.encrypted > file.decrypted
.EE
.PP
Prepare an export of
.I mykey
for use by the person behind GPG ID
.IR bar@example.org ,
whose public key should be accessible in the user's
.BR gpg (1)
keyring.
The key will be exported into the file
.IR export.key :
.PP
.EX
        $ secrecy exportKey mykey bar@example.org > export.key
.EE
.PP
.SH AUTHORS
.PP
.B secrecy
is a program part of the
.BR libsecrecy ,
written by German Tischler-Höhle <germant@miltenyibiotec.de>.
.PP
The present manual page is written by Étienne Mollier <emollier@debian.org> for the Debian project.
.SH SEE ALSO
.PP
See the
.UR https://gnupg.org/documentation/manuals/gnupg/OpenPGP-Key-Management.html#OpenPGP-Key-Management
GnuPG documentation relative to key management
.UE
for creating and handling
.BR gpg (1)
keys.
