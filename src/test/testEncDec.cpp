/**
 * Copyright 2020 German Tischler-Höhle
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
#include <libsecrecy/GCMStreamBase.hpp>
#include <libsecrecy/GPGMEContext.hpp>
#include <libsecrecy/GPGMEVersionCheck.hpp>
#include <libsecrecy/AESEncryptKey.hpp>
#include <libsecrecy/GCMDecrypter.hpp>
#include <libsecrecy/GCMEncrypter.hpp>

#include <libsecrecy/GCMOutputStream.hpp>

#include <iostream>

int main()
{
	try
	{
		bool const ok = libsecrecy::GCMStreamBase::testEncoding(64*1024);

		if ( ok )
		{
			std::cerr << "Encoding test ok" << std::endl;
		}

		libsecrecy::GPGMEVersionCheck gpgvers;
		libsecrecy::GPGMEContext gpgme;
		// gpgme.loadKey(1 /* secret */);

		#if 0
		gpgme.encrypt(std::string("Hello"),std::cout);
		#endif

		libsecrecy::Yarrow Y;

		typedef libsecrecy::AES256EncryptKey key_type;

		std::shared_ptr<key_type> AK(new key_type(Y));
		std::shared_ptr<libsecrecy::GCMIV> IV(new libsecrecy::GCMIV(Y));

		std::cerr << "key hash " << AK->hashToHex() << std::endl;

		uint8_t const authdata[] = { 'A', 'U', 'T', 'H' };
		::std::size_t const l_authdata = sizeof(authdata)/sizeof(authdata[0]);
		std::shared_ptr<uint8_t[]> sauthdata(new uint8_t[l_authdata]);
		std::copy(&authdata[0],&authdata[l_authdata],sauthdata.get());

		uint64_t const countershift = 2;

		std::ostringstream ostr;
		libsecrecy::GCMEncrypter<key_type> enc(ostr,AK,IV,sauthdata,l_authdata,countershift);

		std::string const msg("ABC0DEF1GHI2JKL3" "ABC4DEF5GHI6JKL7");
		enc.write(msg.c_str(),msg.size());
		enc.writeDigest();

		#if 0
		std::cerr << RK << std::endl;
		std::cerr << IV << std::endl;
		#endif

		std::istringstream istr(ostr.str());
		libsecrecy::GCMDecrypter<key_type> dec(istr,AK,IV,sauthdata,l_authdata,countershift);
		libsecrecy::LockedMemory<char> B(msg.size());
		dec.read(B.begin(),msg.size());

		if ( std::equal(msg.begin(),msg.end(),B.begin()) )
		{
			std::cerr << "[V] comparison ok" << std::endl;
		}
		else
		{
			std::cerr << "[E] mismatch between encoded and decoded plaintext" << std::endl;
		}

		dec.readDigest();
		std::cerr << "[V] digest ok" << std::endl;
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
