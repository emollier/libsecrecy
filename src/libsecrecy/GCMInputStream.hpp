/**
 * Copyright 2020 German Tischler-Höhle
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
#if ! defined(LIBSECRECY_GCMINPUTSTREAM_HPP)
#define LIBSECRECY_GCMINPUTSTREAM_HPP

#include <libsecrecy/GCMInputStreamBuffer.hpp>

namespace libsecrecy
{
	/**
	 * input stream for libsecrecy encrypted files. Seeking is supported.
	 **/
	struct GCMInputStream : public GCMInputStreamBuffer, public std::istream
	{
		/**
		 * constructor via std istream object and putbacksize
		 *
		 * @param in input stream object for raw encrypted data
		 * @param putbacksize number of bytes available for putback operation
		 *        (i.e. this many bytes can be read and then be put back into
		 *         the stream and re-read before putback fails)
		 **/
		GCMInputStream(std::istream & in, uint64_t const putbacksize = 0, RawKeyBaseCacheInterface * cache = nullptr)
		: GCMInputStreamBuffer(in,putbacksize,cache), std::istream(this)
		{
		}
	};
}
#endif
