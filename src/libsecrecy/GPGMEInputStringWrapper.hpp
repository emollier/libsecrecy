/**
 * Copyright 2020 German Tischler-Höhle
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
#if ! defined(LIBSECRECY_GPGMEINPUTSTRINGWRAPPER_HPP)
#define LIBSECRECY_GPGMEINPUTSTRINGWRAPPER_HPP

#include <libsecrecy/GPGMEErrorBase.hpp>
#include <cassert>

namespace libsecrecy
{
	struct GPGMEInputStringWrapper : public GPGMEErrorBase
	{
		char const * ca;
		char const * ce;
		gpgme_data_cbs cbs;
		gpgme_data_t data;

		static ssize_t gpgmeread(void * handle, void * buffer, ::std::size_t size)
		{
			GPGMEInputStringWrapper * me = reinterpret_cast<GPGMEInputStringWrapper *>(handle);

			char const * & ca = me->ca;
			char const * & ce = me->ce;

			std::ptrdiff_t const av = ce - ca;

			std::size_t const use = std::min(static_cast<std::size_t>(av),size);

			std::copy(ca,ca + use,reinterpret_cast<char *>(buffer));

			ca += use;

			return use;
		}

		GPGMEInputStringWrapper(
			char const * rca, char const * rce
		) : ca(rca), ce(rce)
		{
			cbs.read = gpgmeread;
			cbs.write = nullptr;
			cbs.seek = nullptr;
			cbs.release = nullptr;

			gpg_error_t const err = gpgme_data_new_from_cbs(&data,&cbs,this);

			if ( err )
			{
				throw std::runtime_error("libsecrecy::GPGMEInputStringWrapper: gpgme_data_new_from_cbs failed "+errorToString(err));
			}
		}

		~GPGMEInputStringWrapper()
		{
			gpgme_data_release(data);
		}
	};
}
#endif
