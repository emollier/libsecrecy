/**
 * Copyright 2020 German Tischler-Höhle
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
#if ! defined(LIBSECRECY_GPGMEOUTPUTSTRINGWRAPPER_HPP)
#define LIBSECRECY_GPGMEOUTPUTSTRINGWRAPPER_HPP

#include <libsecrecy/GPGMEErrorBase.hpp>
#include <libsecrecy/LockedMemory.hpp>
#include <cassert>

namespace libsecrecy
{
	struct GPGMEOutputStringWrapper : public GPGMEErrorBase
	{
		char *       ca;
		char * const ce;
		gpgme_data_cbs cbs;
		gpgme_data_t data;

		static ssize_t gpgmewrite(void * handle, void const * buffer, ::std::size_t size)
		{
			GPGMEOutputStringWrapper * me = reinterpret_cast<GPGMEOutputStringWrapper *>(handle);
			char *       & ca = me->ca;
			char * const & ce = me->ce;

			assert ( ce >= ca );

			std::size_t const av = ce - ca;

			if ( av >= size )
			{
				char const * cbuffer = reinterpret_cast<char const *>(buffer);
				std::copy(cbuffer,cbuffer+size,ca);
				ca += size;
				return size;
			}
			else
			{
				// error no space left on device
				errno = ENOSPC;
				return -1;
			}
		}

		GPGMEOutputStringWrapper(
			char * const rca,
			char * const rce
		) : ca(rca), ce(rce)
		{
			cbs.read = nullptr;
			cbs.write = gpgmewrite;
			cbs.seek = nullptr;
			cbs.release = nullptr;

			gpg_error_t const err = gpgme_data_new_from_cbs(&data,&cbs,this);

			if ( err )
			{
				throw std::runtime_error("libsecrecy::GPGMEOutputStringWrapper: gpgme_data_new_from_cbs failed "+errorToString(err));
			}
		}

		~GPGMEOutputStringWrapper()
		{
			gpgme_data_release(data);
		}
	};
}
#endif
