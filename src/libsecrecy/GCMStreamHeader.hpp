/**
 * Copyright 2020 German Tischler-Höhle
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **/
#if ! defined(LIBSECRECY_GCMSTREAMHEADER_HPP)
#define LIBSECRECY_GCMSTREAMHEADER_HPP

#include <libsecrecy/SecrecyKeyValueStore.hpp>
#include <libsecrecy/GCMIV.hpp>

namespace libsecrecy
{
	struct GCMStreamHeader : public SecrecyKeyValueStore
	{
		std::map<std::string,std::string> M;

		static char const * getMagic()
		{
			return "LIBSECRECY";
		}

		GCMStreamHeader()
		{}

		GCMStreamHeader(std::istream & in)
		: SecrecyKeyValueStore(in,getMagic())
		{

		}

		std::pair < std::shared_ptr<uint8_t[]>, std::size_t > getAuthData() const
		{
			return decodeHexFieldAsArray("authdata");
		}

		std::shared_ptr<GCMIV> getIV() const
		{
			std::vector<uint8_t> const V(decodeHexField("IV"));
			std::shared_ptr<GCMIV> IV(new GCMIV);
			IV->set(V.begin(),V.end());
			return IV;
		}

		std::size_t serialise(std::ostream & out) const
		{
			std::size_t r = 0;

			r += SecrecyKeyValueStore::serialise(out,getMagic());

			return r;
		}

		std::size_t size() const
		{
			return SecrecyKeyValueStore::size(getMagic());
		}
	};
}
#endif
