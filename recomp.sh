#!/bin/bash
rm -fR config.* ltmain.sh aclocal.m4 autom4te.cache compile configure depcomp install-sh missing \
	Makefile Makefile.in src/Makefile src/Makefile.in m4/lt* m4/libtool*

PAR=`egrep "processor[[:space:]]*:" </proc/cpuinfo | wc -l`
autoreconf -i -f
make distclean
${SHELL} doconfig.sh $*
make -j${PAR}
